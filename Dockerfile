# Étape de construction utilisant Maven
FROM maven:3-jdk-8-alpine as builder

# Définir le répertoire de travail
WORKDIR /usr/src/app

# Copier tout le contenu du projet dans le répertoire de travail
COPY . /usr/src/app

# Exécuter Maven pour construire le projet
RUN mvn clean package

# Étape finale utilisant une image légère de Java Runtime
FROM openjdk:8-jre-alpine

# Copier le jar généré depuis l'étape de construction
COPY --from=builder /usr/src/app/target/*.jar /app.jar

# Exposer le port sur lequel l'application sera accessible
EXPOSE 8080

# Définir le point d'entrée de l'application
ENTRYPOINT ["java"]
CMD ["-jar", "/app.jar"]
